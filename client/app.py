import grpc
import helloworld_pb2
import helloworld_pb2_grpc
channel = grpc.insecure_channel("localhost:50051")

stub = helloworld_pb2_grpc.GreeterStub(channel)

# res=helloworld_pb2.HelloRequest(name="roopa")
# response = stub.SayHello(res)
# print(response)

import time
while True:
    res=helloworld_pb2.HelloRequest(name="roopa")
    response = stub.SayHello(res)
    time.sleep(2)