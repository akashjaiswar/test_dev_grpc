var PROTO_FILE_PATH = 'helloworld.proto'


const protoLoader = require('@grpc/proto-loader')

const grpc = require('@grpc/grpc-js');


const packageDefinition = protoLoader.loadSync(PROTO_FILE_PATH, {
    keepCase: true,
    longs: String,
    enums: String,
    defaults: true,
    oneofs: true
})

const helloProto = grpc.loadPackageDefinition(packageDefinition).helloworld;

function sayHello(call, callback) {
    console.log("from client",call.request.name)
    callback(null, { greeting: 'Hello' + call.request.name });
}

function main() {
    console.log("reached")

    const server = new grpc.Server();
    server.addService(helloProto.Greeter.service, { sayHello: sayHello });

    server.bindAsync('0.0.0.0:50051', grpc.ServerCredentials.createInsecure(), (err, res) => {
        if (res) {
            server.start();
        }
    });
}

main();
















